# syntax=docker/dockerfile:1
FROM python:3.10.1

WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "-m", "u06.src.main" ]