"""Pydantic/SQLModel models"""
from datetime import datetime
from typing import Optional
from uuid import uuid4

from pydantic import BaseModel

from sqlalchemy import String, Integer
from sqlalchemy.sql.schema import Column

from sqlmodel import Field, SQLModel


class Discounts(SQLModel, table=True):
    id: str = Field(default=uuid4, primary_key=True)
    product: str = Field(default=uuid4, sa_column=Column("product", String, unique=True), foreign_key="products.id")
    discount_percent: int


class InventoryBase(SQLModel):
    product: str = Field(default=uuid4, sa_column=Column("product", String, unique=True), foreign_key="products.id")
    quantity: int
    store: str = Field(default=uuid4, foreign_key="stores.id")


class Inventory(InventoryBase, table=True):
    id: str = Field(default=uuid4, primary_key=True)


class Prices(SQLModel, table=True):
    id: str = Field(default=uuid4, primary_key=True)
    product: str = Field(default=uuid4, sa_column=Column("product", String, unique=True), foreign_key="products.id")
    price: str  # condecimal(max_digits=6, decimal_places=2)


class Products(SQLModel, table=True):
    id: str = Field(default=uuid4, primary_key=True)
    article_nr: int = Field(default=uuid4, sa_column=Column("article_nr", Integer, unique=True))
    name: str = Field(default=uuid4, sa_column=Column("name", String, unique=True))
    description: Optional[str] = Field(None)


class Sales(SQLModel, table=True):
    id: str = Field(default=uuid4, primary_key=True)
    timestamp: Optional[str] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    store: str = Field(default=uuid4, foreign_key="stores.id")


class SoldProductsBase(SQLModel):
    product: str = Field(default=uuid4, sa_column=Column("product", String, unique=True), foreign_key="products.id")
    sale: str = Field(default=uuid4, sa_column=Column("sale", String, unique=True), foreign_key="sales.id")
    quantity: int


class SoldProducts(SoldProductsBase, table=True):
    id: str = Field(default=uuid4, primary_key=True)


class StoreAddresses(SQLModel, table=True):
    id: str = Field(default=uuid4, primary_key=True)
    store: str = Field(default=uuid4, foreign_key="stores.id")
    address: str
    zipcode: str
    city: str


class Stores(SQLModel, table=True):
    id: str = Field(default=uuid4, primary_key=True)
    name: str = Field(default=uuid4, sa_column=Column("name", String, unique=True))


class Order(BaseModel):
    """
    Pydantic model for validation of data collected from .xls(x) files
    """
    storename: str
    storeaddress: str
    storezip: str
    storecity: str
    prods: list[int]
    qty: list[int]
    order_name: str
