"""Holds the database information"""

from sqlmodel import create_engine

from .config import settings

SQLITE_FILE_NAME = f'{settings.db_path}database.db'
sqlite_url = f'sqlite:///{SQLITE_FILE_NAME}'

engine = create_engine(sqlite_url, echo=False)
