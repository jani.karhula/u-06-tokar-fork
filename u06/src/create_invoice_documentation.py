import logging
from decimal import Decimal

from pandas import DataFrame

from sqlmodel import Session

from ..config import settings
from ..database import engine
from ..src.crud import get_price
from ..models import Order


def data_to_csv(order: Order, dest_path=settings.dest_path_csv):
    """
    Write all collected data to .csv
    Prices are fetched from database and VAT (25%) is calculated.
    :param dest_path:
    :param order
    """
    with Session(engine) as session:
        prices, vat = [], []
        for article_nr in order.prods:
            price = get_price(session=session, article_nr=article_nr)
            prices.append(price)
            vat.append(Decimal(price * .25))
        df = DataFrame({'Namn': order.storename,
                        'Fakturaadress': order.storeaddress,
                        'Postnr': order.storezip,
                        'Ort': order.storecity,
                        'Produkt': order.prods,
                        'Antal': order.qty,
                        'Pris exkl. moms': prices,
                        'Moms': vat})
        df.to_csv(rf'{dest_path}{order.order_name.rstrip(".xls")}.csv', index=False)
        logging.info(f'Order info exported to {dest_path}{order.order_name.rstrip(".xls")}.csv')
