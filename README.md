# U06 - Tomas Karlsson  

Syftet med detta projekt är att automatisera en tidskrävande manuell process:  
Kunder mejlar en Excelfil med sin beställning.
Istället för att manuellt skriva in beställningar i orderhanteringssystemet ska ett skript köras som parsar .xls(x)-filer, skriver in nödvändig data till databasen och spottar ut en CSV-fil som kan importeras till ett faktureringssystem.
Jag utgår från databasen i u05 (migrerad till SQLite) och låter den styra vilken data som behövs.

## Sprint 1  
Fokus i sprint 1 är att skapa en MVP som hanterar huvuddelen i appen - att analysera .xls(x)-filer, skriva till databasen samt exportera ett fakturaunderlag i CSV-format.  
Om tid finns, se till att appen körs som en daemon.  
  
- Databas  
  - Migrera PostgreSQL till SQLite  
  - Skapa modeller för alla relationer i databasen  
- Excelimport  
  - Ska kunna processa Excelfiler i två olika format  
  - Analysera datastrukturen och processa data  
- Konfigurationsfil  
  - Sökvägar för import och export av PDF-filer  
  - Sökväg för export av CSV-filer  
- CSV-export  
  - Exportera all ny data i CSV-format för import till faktureringssystem    
- Daemonisera appen  
  - Använd docker för att göra appen plattformsoberoende  


## Sprint 2  

I sprint 2 ligger fokus på två saker:  
- Vidareutveckla appen så att den kan hantera ett tredje excelformat  
- Skapa ett CLI-interface för lagerpersonal.  

Detaljerad överblick över sprint 2:  
- Excelimport  
  - Lägg till funktionalitet för ett tredje orderformat - excel binary(xlsb)
- CLI-frontend för lagerpersonal  
  - Användaren ska kunna få fram en lista på alla ordrar som inte har status packad  
    - Listan bör visa(åtminstone) butikens namn och datum då ordern registrerats.
    - Lägg till en kolumn i databasen (table: sales) där orderstatus kan registreras  
      - Fyra möjliga alternativ för orderstatus ska finnas: ny, hanteras, packad, klar
  - Användaren ska kunna välja en specifik order från listan och få fram all information om denna:  
    - _Butikens namn_
    - _Full adress_
    - En lista över _Produkter och kvantitet_
      - Status uppdateras till "hanteras" när en order "öppnas"
  - Användaren ska kunna ändra status på en specifik order som "packad" OM den har status "hanteras"  
    - När en order är markerad som packad ska databasen uppdateras med denna status  
  - Alternativ för att skapa plocklista i Excelformat eller CSV  

## Filstruktur
- __u06/src/main.py__  
Huvuddelen av appen
- __u06/src/create_invoice_documentation.py__  
Denna fil sköter skrivandet av CSV-filer  
- __u06/src/crud.py__  
Här finns alla funktioner som skriver, läser, uppdaterar och raderar data från databasen (crus = Create, Read, Update, Delete)


- __u06/config.py__  
Konfigurationsfil för appen. Här ska variabler för konfigurering av appen finnas. Ska vara väl dokumenterad  
- __u06/database.py__  
Modul för hantering av databasanslutning  
- __u06/models.py__  
Modul innehållandes alla SQLModel-modeller (som motsvarar alla tabeller i databasen)  


- __tests/__  
Innehåller alla tester(pytest) samt fixtures för testerna


- __docker-compose.yaml__  
Instruktioner för docker-compose. Sätter korrekt tidzzon samt mountar alla volymer  
- __Dockerfile__  
Instruktioner för docker  
- __.env__  
Innehåller environment-variabler som används i appen och i docker-compose.yaml

### Dependencies
- SQL Model  
  Används för att interagera med databasen (SQL Model är en ORM - Object-Relational Mapper)  
  https://sqlmodel.tiangolo.com/  
- Pydantic  
  Används för att skapa datamodeller  
  https://pydantic-docs.helpmanual.io/  
- Pandas  
  Används(bland annat) för att läsa och skriva excel- och csv-filer  
  https://pandas.pydata.org/  
  - xlrd  
  Används för att läsa det äldre xls-formatet  
  https://xlrd.readthedocs.io/en/latest/  
  - openpyxl  
  https://openpyxl.readthedocs.io/en/stable/  
  Används för att läsa in det nyare xlsx-formatet
- Watchdog
  Används för att övervaka ett filsystem  
  https://pypi.org/project/watchdog/  


Utöver dessa används följande bibliotek från standard library:
- decimal  
  https://docs.python.org/3/library/decimal.html  
- logging  
  https://docs.python.org/3/howto/logging.html  
- shutil  
  https://docs.python.org/3/library/shutil.html  
- time  
  https://docs.python.org/3/library/time.html

# Användning av appen - Observer

### Konfiguration  
Appen använder en ```.env```-fil för att läsa in path till följande mappar:
- dump
  - Den mappen som appen bevakar. När en excelfil finns i denna mapp triggas funktioner i appen.
- archive
  - Hit flyttas excelfilerna när appen har hanterat dem.
- csv
  - CSV-filer skrivs till denna mapp
- db
  - Här ligger SQLite-databasen

Så första steget när denna app ska utvecklas eller användas är att flytta innehållet i mappen ```app_files``` till önskvärd plats samt att skapa en .env fil i projektets rotmapp.  
Denna fil ska innehålla följande:
```
SOURCE_PATH = 'PATH TILL/dump/'
DEST_PATH_ORDERS = 'PATH TILL/archive/'
DEST_PATH_CSV = 'PATH TILL/csv/'
DB_PATH = 'PATH TILL/db/'
```
(Ersätt alltså 'PATH TILL' till den absoluta sökvägen för var och en av mapparna som appen behöver använda)

### Utvecklarmiljö
1. Installera python 3.10  
   ```python3 --version``` för att se vilken version som finns installerat på ditt system.  
   Om versionen är lägre än 3.10 så kan du ladda ner python 3.10 här:  
https://www.python.org/downloads/  
2. Skapa en virtuell miljö i rotmappen med venv exempelvis: ```python3 -m venv venv```  
3. Installera alla dependencies från requirements.txt: ```pip install -r requirements.txt```  
4. För att köra appen i ett terminalfönster utgår du från rotnivån och kör: ```python3 -m u06.src.main```


## Docker  
Appen är tänkt att fungera som en daemon som bevakar en mapp så länge systemet är igång. För att vara plattformsoberoende körs den dock inte som en daemon utan den körs i en dockercontainer (som går att konfigurera så den alltid startas upp om den stoppas eller kraschar).  

För att sätta upp appen i en dockercontainer så kör du följande kommando från den mapp där docker-compose.yaml samt Dockerfile finns i ett terminalfönster: ```docker-compose up --build```  
Det kommer se ut som följande när dockercontainern är startad: 
  ```
$ docker-compose up --build
Building observer
[+] Building 6.3s (14/14) FINISHED                                                                             
 => [internal] load build definition from Dockerfile                                                      0.0s
 => => transferring dockerfile: 37B                                                                       0.0s
 => [internal] load .dockerignore                                                                         0.0s
 => => transferring context: 2B                                                                           0.0s
 => resolve image config for docker.io/docker/dockerfile:1                                                0.7s
 => CACHED docker-image://docker.io/docker/dockerfile:1@sha256:91f386bc3ae6cd5585fbd02f811e295b4a7020c23  0.0s
 => [internal] load .dockerignore                                                                         0.0s
 => [internal] load build definition from Dockerfile                                                      0.0s
 => [internal] load metadata for docker.io/library/python:3.10.1                                          0.7s
 => [internal] load build context                                                                         3.7s
 => => transferring context: 908.85kB                                                                     3.6s
 => [1/5] FROM docker.io/library/python:3.10.1@sha256:b39f8b98308263406dc266ce9244f2e00f0d367003b401543d  0.0s
 => CACHED [2/5] WORKDIR /app                                                                             0.0s
 => CACHED [3/5] COPY requirements.txt ./                                                                 0.0s
 => CACHED [4/5] RUN pip install --no-cache-dir -r requirements.txt                                       0.0s
 => CACHED [5/5] COPY . .                                                                                 0.0s
 => exporting to image                                                                                    0.3s
 => => exporting layers                                                                                   0.0s
 => => writing image sha256:d4216407bd0e03a0165ddb0b399aeedba0b6689e46fac10e4d512f2b8558ad74              0.0s
 => => naming to docker.io/library/observer                                                               0.0s
Starting u06_observer_1 ... done
Attaching to u06_observer_1
```
Sökvägarna från ```.env``` är mountade i docker containern. Detta innebär att appen bevakar en mapp (dump) i containern. I och med att detta är en mountad volym så bevakas i praktiken även mappen(dump) lokalt.