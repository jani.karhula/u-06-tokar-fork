import logging
from shutil import copy

from sqlmodel import Session
from sqlmodel.sql.expression import Select, SelectOfScalar

from u06.models import Inventory
from u06.src.main import parse_order, Order, find_and_load_orders, handle_sale

# Workaround for SAWarcning in SQLModel (discussed here: https://github.com/tiangolo/sqlmodel/issues/189)
SelectOfScalar.inherit_cache = True
Select.inherit_cache = True


def test_parse_order(caplog):
    # Set pytest to grab logging by INFO level

    with caplog.at_level(logging.INFO):
        # GIVEN a proper excel file
        file = "tests/fixtures/testorder.xlsx"

        # WHEN running parse_order
        order = parse_order(file)

        # THEN the function returns an instance of Order and the logged output is as expected.
        assert isinstance(order, Order)
        assert 'Found order from Den Lilla Djurbutiken' in caplog.text


def test_parse_broken_order(caplog, tmpdir):
    # Set pytest to grab logging by ERROR level
    with caplog.at_level(logging.ERROR):

        # GIVEN a broken excel file
        sourcepath = tmpdir.mkdir('test_parse_broken_order')
        tmpdir.mkdir('test_parse_broken_order/corrupt')
        copy('tests/fixtures/testorder_broken.xls', f'{sourcepath}')
        file = f"{sourcepath}/testorder_broken.xls"

        # WHEN running parse_order
        order = parse_order(filename=file, sourcepath=f'{sourcepath}/')

        # THEN order is not an instance of Order, an exception is being raised and logged output is as expected
        assert not isinstance(order, Order)
        assert ValueError('Excel file format cannot be determined, you must specify an engine manually.')
        assert 'ERROR' in caplog.text


def test_parse_proper_excel_but_not_an_order(caplog, tmpdir):
    # Set pytest to grab logging by ERROR level
    with caplog.at_level(logging.ERROR):

        # GIVEN a proper excel file that is not in a known order format
        sourcepath = tmpdir.mkdir('test_parse_broken_order')
        tmpdir.mkdir('test_parse_broken_order/corrupt')
        copy('tests/fixtures/not_an_order.xlsx', f'{sourcepath}')
        file = f"{sourcepath}/not_an_order.xlsx"

        # WHEN running parse_order
        order = parse_order(filename=file, sourcepath=f'{sourcepath}/')

        # THEN order is not an instance of Order, an exception is being raised and logged output is as expected
        assert not isinstance(order, Order)
        assert KeyError('Order form')
        assert 'ERROR' in caplog.text


def test_find_and_load_orders(caplog, mocker, tmpdir, session: Session):
    # Set pytest to grab logging by INFO level
    with caplog.at_level(logging.INFO):

        # GIVEN a proper excel file
        mocker.patch('u06.src.main.handle_sale')
        sourcepath = tmpdir.mkdir('test_find_and_load_orders')
        destpath = tmpdir.mkdir('test_find_and_load_orders/archive')
        copy('tests/fixtures/testorder.xlsx', f'{sourcepath}')

        # WHEN running find_and_load_orders
        order = find_and_load_orders(sourcepath=f'{sourcepath}/', destpath=f'{destpath}/')

        # THEN the first object in order is an instance of Order and logged output is as expected
        assert isinstance(order[0], Order)
        assert 'Found order from Den Lilla Djurbutiken' and 'testorder.xlsx moved to' in caplog.text


def test_handle_sale(caplog, mocker, session: Session):
    # Set pytest to grab logging by INFO level
    with caplog.at_level(logging.INFO):

        # GIVEN an empty database and data parsed from an order
        store_name = 'Den Lilla Djurbutiken'
        products = [511]
        quantities = [1]
        mocker.patch('u06.src.main.get_store_id', return_value='ff53d831-c2fe-4fe8-9f67-5d69118670f2')
        mocker.patch('u06.src.main.add_sale', return_value='xx53d831-c2fe-4fe8-9f67-5d69118670f2')
        mocker.patch('u06.src.main.get_product_id', return_value='eb4d618c-122d-4428-b022-38aa1ad36fe0')
        mocker.patch('u06.src.main.add_sold_products', return_value='xx4d618c-122d-4428-b022-38aa1ad36fe0')
        mocker.patch('u06.src.main.get_products_from_inventory', return_value=['eb4d618c-122d-4428-b022-38aa1ad36fe0'])
        mocker.patch('u06.src.main.get_inventory',
                     return_value=Inventory(id='xx53d831-c2fe-4fe8-9f67-5d69118670f2',
                                            product='eb4d618c-122d-4428-b022-38aa1ad36fe0',
                                            quantity=1,
                                            store='ff53d831-c2fe-4fe8-9f67-5d69118670f2'))
        mocker.patch('u06.src.crud.update_inventory')

        # WHEN running handle_sale
        handle_sale(session=session, store_name=store_name, products=products, quantities=quantities)

        # THEN logged output is as expected
        assert 'Order written to database' in caplog.text
